package com.codenjoy.dojo.bomberman;

import com.codenjoy.dojo.bomberman.board.SilentBoard;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchSolver;
import com.codenjoy.dojo.bomberman.logic.asearch.strategy.BombersHunter;
import com.codenjoy.dojo.client.WebSocketRunner;

public class Main {

    public static void main(String[] args) {
        String player = "p0p8n9fwffdxem12bje0";
        String code = "";
        WebSocketRunner.PRINT_TO_CONSOLE = false;
        BombersHunter strategy = new BombersHunter();
        WebSocketRunner.runClient(
                String.format("https://botchallenge.cloud.epam.com/codenjoy-contest/board/player/%s?code=%s", player,
                        code), new ASearchSolver(strategy, strategy), new SilentBoard());
    }
}
