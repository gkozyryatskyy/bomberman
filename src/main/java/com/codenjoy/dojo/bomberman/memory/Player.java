package com.codenjoy.dojo.bomberman.memory;

import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_BOMBERMAN;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.atomic.AtomicInteger;

import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.model.Elements;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(onlyExplicitlyIncluded = true)
public class Player {

    public static final int PERK_TIME = 30;
    public static final int BOMB_RADIUS_INCREASE = 2;
    public static final int DETONATORS_INCREASE = 3;

    public static final int DEFAULT_BOMB_RADIUS = 3;
    public static final int DEFAULT_DETONATORS = 0;

    @ToString.Include
    private final Cell cell;
    private final Map<Cell, AtomicInteger> bombs = new HashMap<>();
    // active perks
    // BOMB_BLAST_RADIUS_INCREASE
    @ToString.Include(name = "RadiusT")
    private int boomRadiusTimer = 0;
    @ToString.Include(name = "Radius")
    private int boomRadius = DEFAULT_BOMB_RADIUS;

    // BOMB_COUNT_INCREASE
    @ToString.Include(name = "CountT")
    private int boomCountTimer = 0;
    // BOMB_IMMUNE
    @ToString.Include(name = "ImmuneT")
    private int boomImmuneTimer = 0;
    // BOMB_REMOTE_CONTROL
    @ToString.Include(name = "det")
    private int detonators = DEFAULT_DETONATORS;
    @Getter
    @ToString.Include(name = "rc")
    private Cell rcBomb;
    @Getter
    private AtomicInteger rcBombTimer;

    public Player(Player previousPlayer, Memory previousTurn, Memory currentTurn, Cell player) {
        this.cell = player;
        // if bomberman on a bomb now, this is player bomb
        if (player.isBomb()) {
            this.bombs.put(player, new AtomicInteger(4));
        }
        // merge data from prev player object, should be before prevTurn check
        if (previousPlayer != null) {
            previousPlayer.tick();
            previousPlayer.getBombs().forEach((c, t) -> {
                Cell cell = currentTurn.get(c.getX(), c.getY());
                // bomb tick.. move to tick() method?
                t.decrementAndGet();
                if (cell.isBomb()) {
                    // adding this turn cell
                    this.bombs.put(cell, t);
                }
                //support MEAT_CHOPPER on a bomb, do not loose a bomb
                if (Elements.MEAT_CHOPPER.equals(cell.getE()) || Elements.DEAD_MEAT_CHOPPER.equals(cell.getE())) {
                    if (t.get() > 0) {
                        this.bombs.put(cell, t);
                    }
                }
            });
            // if there is a bomb now on prev player place, this is player bomb
            Cell cell = currentTurn.get(previousPlayer.getCell().getX(), previousPlayer.getCell().getY());
            if (cell.isBomb()) {
                // this bomb can already exists
                this.bombs.putIfAbsent(cell, new AtomicInteger(4));
            }
            this.boomRadiusTimer = previousPlayer.boomRadiusTimer;
            this.boomRadius = previousPlayer.boomRadius;
            this.boomCountTimer = previousPlayer.boomCountTimer;
            this.boomImmuneTimer = previousPlayer.boomImmuneTimer;
            this.detonators = previousPlayer.detonators;
            AtomicInteger prevRcBombTimer = this.bombs.get(previousPlayer.rcBomb);
            if (prevRcBombTimer != null) {
                this.rcBomb = previousPlayer.rcBomb;
                this.rcBombTimer = prevRcBombTimer;
            }
        }
        // add new perks data
        if (previousTurn != null) {
            // check me on the previous perk
            Cell prevCell = previousTurn.get(player.getX(), player.getY());
            // check for active perks
            if (Cell.PERKS.contains(prevCell.getE())) {
                // add for 10 turns
                switch (prevCell.getE()) {
                    case BOMB_BLAST_RADIUS_INCREASE:
                        this.boomRadiusTimer += PERK_TIME;
                        this.boomRadius += BOMB_RADIUS_INCREASE;
                        break;
                    case BOMB_COUNT_INCREASE:
                        this.boomCountTimer = PERK_TIME;
                        break;
                    case BOMB_IMMUNE:
                        this.boomImmuneTimer = PERK_TIME;
                        break;
                    case BOMB_REMOTE_CONTROL:
                        this.detonators = DETONATORS_INCREASE;
                        break;
                    default:
                        break;
                }
            }
        }
        // try to find RC bomb
        if (rcBomb == null && detonators > 0) {
            Map.Entry<Cell, AtomicInteger> rcBombEntry = bombs
                    .entrySet()
                    .stream()
                    .filter(e -> BOMB_BOMBERMAN.equals(e.getKey().getE()) || BOMB_BOMBERMAN.equals(e.getKey().getE()))
                    .findAny()
                    .orElse(null);
            if (rcBombEntry != null) {
                rcBomb = rcBombEntry.getKey();
                rcBombTimer = rcBombEntry.getValue();
            }
        }
    }

    public void tick() {
        // add bomb tick from constructor?
        if (boomRadiusTimer > 0) {
            this.boomRadiusTimer--;
            if (boomRadiusTimer <= 0) {
                this.boomRadius = DEFAULT_BOMB_RADIUS;
            }
        }
        if (boomCountTimer > 0) {
            this.boomCountTimer--;
        }
        if (boomImmuneTimer > 0) {
            this.boomImmuneTimer--;
        }
    }

    public void useDetonator() {
        if (detonators > 0) {
            this.detonators--;
        }
    }

    @SuppressWarnings("BooleanMethodIsAlwaysInverted")
    public boolean isImmune() {
        return this.boomImmuneTimer > 3;
    }

}
