package com.codenjoy.dojo.bomberman.memory;

import static com.codenjoy.dojo.bomberman.model.Elements.BOMBERMAN;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_BOMBERMAN;
import static com.codenjoy.dojo.bomberman.model.Elements.DEAD_BOMBERMAN;

import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.stream.Collectors;

import com.codenjoy.dojo.bomberman.client.Board;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.model.ElementGroup;
import com.codenjoy.dojo.bomberman.model.Elements;

import lombok.Getter;

public class Memory {

    private final int size;
    private final Cell[][] cells; // current round board
    private final Map<Elements, List<Cell>> cache; // current round elements cache
    private final Map<ElementGroup, Set<Cell>> cache1; // current round elements cache
    @Getter
    private final LongMemory longMemory;

    public Memory(Board board, LongMemory longMemory) {
        char[][] field = board.getField();
        this.size = board.size();
        this.cells = new Cell[size][size];
        for (int x = 0; x < field.length; x++) {
            for (int y = 0; y < field[0].length; y++) {
                // add cell
                this.cells[x][y] = new Cell(x, y, board.getAt(x, y), this);
            }
        }
        this.cache = new HashMap<>();
        this.cache1 = new HashMap<>();
        this.longMemory = longMemory;
        longMemory.tick(this);
    }

    public Cell get(int x, int y) {
        if (x < 0 || y < 0 || y > size - 1 || x > size - 1) {
            // stub for coordinated out of field
            return new Cell(x, y, Elements.WALL, this);
        } else {
            return cells[x][y];
        }
    }

    // keep the order of perk groups!
    public List<Cell> get(Collection<Elements> elements) {
        return elements.stream().flatMap(e -> get(e).stream()).collect(Collectors.toList());
    }

    public Set<Cell> get(Elements... elements) {
        return Arrays.stream(elements).flatMap(e -> get(e).stream()).collect(Collectors.toSet());
    }

    public List<Cell> get(Elements element) {
        List<Cell> retval = cache.get(element);
        if (retval == null) {
            retval = get0(element);
            cache.put(element, retval);
        }
        return retval;
    }

    private List<Cell> get0(Elements element) {
        List<Cell> retval = Arrays.stream(cells)
                .flatMap(Arrays::stream)
                .filter(e -> element.equals(e.getE()))
                .collect(Collectors.toList());
        switch (element) {
            case BOMB_TIMER_1:
                longMemory.iterateHiddenBombs(1, retval::add);
                break;
            case BOMB_TIMER_2:
                longMemory.iterateHiddenBombs(2, retval::add);
                break;
            case BOMB_TIMER_3:
                longMemory.iterateHiddenBombs(3, retval::add);
                break;
            case BOMB_TIMER_4:
                longMemory.iterateHiddenBombs(4, retval::add);
                break;
            case BOMB_TIMER_5:
                longMemory.iterateHiddenBombs(5, retval::add);
                break;
            default:
                break;
        }
        return retval;
    }

    // --------------------------------- groups ---------------------------------

    public Set<Cell> get(ElementGroup element) {
        Set<Cell> retval = cache1.get(element);
        if (retval == null) {
            retval = element.getFunc().apply(this);
            cache1.put(element, retval);
        }
        return retval;
    }

    public Set<Cell> getBombs() {
        return get(ElementGroup.BOOMBS);
    }

    public Set<Cell> getOtherBombers() {
        return get(ElementGroup.OTHER_BOMBERS);
    }

    // --------------------------------- definitions ---------------------------------

    public Cell getMe() {
        return get(BOMBERMAN, BOMB_BOMBERMAN, DEAD_BOMBERMAN).stream().findAny().orElse(null);
    }
}
