package com.codenjoy.dojo.bomberman.memory;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Consumer;
import java.util.stream.Collectors;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.model.Elements;

import lombok.Getter;
import lombok.ToString;

@ToString
public class LongMemory {

    // using 5 size, because new round starts with 5 counts
    // first is older
    @ToString.Exclude
    private final CircularFifoQueue<Memory> memoryQueue = new CircularFifoQueue<>(5);
    @Getter
    private Player me;
    @Getter
    private List<Player> others = new ArrayList<>();
    // TODO change hidden bombs to player bombs
    @Getter
    private final Map<Cell, AtomicInteger> hiddenBombs = new HashMap<>();

    public void tick(Memory memory) {
        Cell meCell = memory.getMe();
        if (!Elements.DEAD_BOMBERMAN.equals(meCell.getE()) && !memoryQueue.isEmpty()) {
            Memory prev = memoryQueue.get(memoryQueue.size() - 1);
            // check me on the previous perk
            this.me = new Player(me, prev, memory, meCell);
            // merger prev other players with their new cells
            this.others = memory.getOtherBombers().stream()
                    .map(c -> {
                        Set<Cell> connections = c.getConnections();
                        connections.add(c);
                        Player prevPlayer = null;
                        Iterator<Player> it = others.iterator();
                        while (it.hasNext()) {
                            prevPlayer = it.next();
                            if (connections.contains(prevPlayer.getCell())) {
                                it.remove();
                                break;
                            }
                        }
                        return new Player(prevPlayer, prev, memory, c);
                    }).collect(Collectors.toList());
            // check for hidden bombs
            memory.get(Elements.OTHER_BOMB_BOMBERMAN).forEach(c -> {
                hiddenBombs.putIfAbsent(c, new AtomicInteger(5));
            });
            // check for hidden bombs from choppers
            memory.get(Elements.MEAT_CHOPPER, Elements.DEAD_MEAT_CHOPPER).forEach(c -> {
                Cell prevCell = prev.get(c.getX(), c.getY());
                switch (prevCell.getE()){
                    case BOMB_TIMER_2:
                        hiddenBombs.put(prevCell, new AtomicInteger(2));
                        break;
                    case BOMB_TIMER_3:
                        hiddenBombs.put(prevCell, new AtomicInteger(3));
                        break;
                    case BOMB_TIMER_4:
                        hiddenBombs.put(prevCell, new AtomicInteger(4));
                        break;
                    case BOMB_TIMER_5:
                        // using init val 6, because it should be RC bomb and we should not decrement it to 4
                        hiddenBombs.put(prevCell, new AtomicInteger(6));
                        break;
                    default:
                        break;
                }
            });
        } else {
            this.me = new Player(null, null, memory, meCell);
            this.others = memory.getOtherBombers()
                    .stream()
                    .map(c -> new Player(null, null, memory, c))
                    .collect(Collectors.toList());
        }
        // decrease all hiddenBombs for 1 turn and cleanup
        hiddenBombs.forEach((k, v) -> v.getAndDecrement());
        hiddenBombs.entrySet().removeIf(e -> e.getValue().get() <= 0);
        // add current mem to memoryQueue
        memoryQueue.add(memory);
    }

    public Player bombOwner(Cell bomb) {
        AtomicInteger timer = me.getBombs().get(bomb);
        if (timer != null) {
            return me;
        } else {
            for (Player other : others) {
                timer = other.getBombs().get(bomb);
                if (timer != null) {
                    return other;
                }
            }
            return null;
        }
    }

    public void iterateHiddenBombs(int timer, Consumer<Cell> consumer) {
        hiddenBombs.forEach((k, v) -> {
            if (v.get() == timer) {
                consumer.accept(k);
            }
        });
    }
}
