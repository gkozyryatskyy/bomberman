package com.codenjoy.dojo.bomberman.model;

import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.memory.Memory;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public enum MoveCommand {

    LEFT(-1, 0), RIGHT(1, 0), UP(0, 1), DOWN(0, -1);

    private int x;
    private int y;

    public Cell move(Memory mem, Cell cell) {
        return mem.get(cell.getX() + x, cell.getY() + y);
    }


    public static MoveCommand getCommand(Cell from, Cell to) {
        if (from.getX() < to.getX()) {
            return RIGHT;
        } else if (from.getX() > to.getX()) {
            return LEFT;
        } else if (from.getY() < to.getY()) {
            return UP;
        } else if (from.getY() > to.getY()) {
            return DOWN;
        } else {
            return null;
        }
    }
}
