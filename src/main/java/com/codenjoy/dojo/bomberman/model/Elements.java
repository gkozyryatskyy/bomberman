package com.codenjoy.dojo.bomberman.model;

/*-
 * #%L
 * Codenjoy - it's a dojo-like platform from developers to developers.
 * %%
 * Copyright (C) 2018 Codenjoy
 * %%
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public
 * License along with this program.  If not, see
 * <http://www.gnu.org/licenses/gpl-3.0.html>.
 * #L%
 */

import com.codenjoy.dojo.services.printer.CharElements;

import lombok.Getter;

public enum Elements implements CharElements {
    // NOTE!!! we add canBoomThrough = true for all BOMBERMAN and BOMB_BOMBERMAN for BombersHunter.act function to see back
    /// This is your Bomberman
    BOMBERMAN('☺', true, true),             // this is what he usually looks like
    BOMB_BOMBERMAN('☻', false, true),        // this is if he is sitting on own bomb
    DEAD_BOMBERMAN('Ѡ', false,
            true),        // oops, your Bomberman is dead (don't worry, he will appear somewhere in next move)
    // you're getting penalty points for each death

    /// this is other players Bombermans
    // NOTE!!! we add canBoomThrough = true for all OTHER_BOMBERMAN and OTHER_BOMB_BOMBERMAN because they most likely go away from the boom
    OTHER_BOMBERMAN('♥', false, true),       // this is what other Bombermans looks like
    OTHER_BOMB_BOMBERMAN('♠', false, true),  // this is if player just set the bomb
    OTHER_DEAD_BOMBERMAN('♣', true, true),  // enemy corpse (it will disappear shortly, right on the next move)
    // if you've done it you'll get score points

    /// the bombs
    BOMB_TIMER_5('5', false, true),          // after bomberman set the bomb, the timer starts (5 ticks)
    BOMB_TIMER_4('4', false, true),          // this will blow up after 4 tacts
    BOMB_TIMER_3('3', false, true),          // this after 3
    BOMB_TIMER_2('2', false, true),          // two
    BOMB_TIMER_1('1', false, true),          // one
    BOOM('҉', false,
            true),                  // Boom! this is what is bomb does, everything that is destroyable got destroyed

    /// walls
    WALL('☼', false, false),                  // indestructible wall - it will not fall from bomb
    DESTROYABLE_WALL('#', false, false),      // this wall could be blowed up
    DESTROYED_WALL('H', true, true),        // this is how broken wall looks like, it will dissapear on next move
    // if it's you did it - you'll get score points.

    /// meatchoppers
    MEAT_CHOPPER('&', false, true),          // this guys runs over the board randomly and gets in the way all the time
    // if it will touch bomberman - it will die
    // you'd better kill this piece of ... meat, you'll get score points for it
    DEAD_MEAT_CHOPPER('x', true, true),     // this is chopper corpse

    /// perks
    BOMB_BLAST_RADIUS_INCREASE('+', true,
            true), // Bomb blast radius increase. Applicable only to new bombs. The perk is temporary.
    BOMB_COUNT_INCREASE('c', true,
            true),   // Increase available bombs count. Number of extra bombs can be set in settings. Temporary.
    BOMB_REMOTE_CONTROL('r', true,
            true),   // Bomb blast not by timer but by second act. Number of RC triggers is limited and can be set in settings.
    BOMB_IMMUNE('i', true, true),           // Do not die after bomb blast (own bombs and others as well). Temporary.

    /// a void
    NONE(' ', true, true);                 // this is the only place where you can move your Bomberman

    public static final String BOMBS = "12345";

    final char ch;
    @Getter
    final boolean canGoThrough;
    @Getter
    final boolean canBoomThrough;

    Elements(char ch, boolean canGoThrough, boolean canBoomThrough) {
        this.ch = ch;
        this.canGoThrough = canGoThrough;
        this.canBoomThrough = canBoomThrough;
    }

    @Override
    public char ch() {
        return ch;
    }

    @Override
    public String toString() {
        return String.valueOf(ch);
    }

    public static Elements valueOf(char ch) {
        for (Elements el : Elements.values()) {
            if (el.ch == ch) {
                return el;
            }
        }
        throw new IllegalArgumentException("No such element for " + ch);
    }

    public boolean isBomb() {
        return BOMBS.indexOf(ch) != -1;
    }

    public boolean isBomberman() {
        return this == Elements.BOMBERMAN || this == Elements.BOMB_BOMBERMAN || this == Elements.DEAD_BOMBERMAN;
    }

}
