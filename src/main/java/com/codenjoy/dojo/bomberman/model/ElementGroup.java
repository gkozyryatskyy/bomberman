package com.codenjoy.dojo.bomberman.model;

import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_BOMBERMAN;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_1;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_2;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_3;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_4;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_5;
import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_BOMBERMAN;
import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_BOMB_BOMBERMAN;
import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_DEAD_BOMBERMAN;

import java.util.Set;
import java.util.function.Function;
import java.util.stream.Collectors;

import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.memory.Player;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
@Getter
public enum ElementGroup {

    BOOMBS(m -> m.get(BOMB_TIMER_1, BOMB_TIMER_2, BOMB_TIMER_3, BOMB_TIMER_4, BOMB_TIMER_5, BOMB_BOMBERMAN,
            OTHER_BOMB_BOMBERMAN)),

    OTHER_BOMBERS(m -> m.get(OTHER_BOMBERMAN, OTHER_BOMB_BOMBERMAN, OTHER_DEAD_BOMBERMAN)),

    BOOM_1(getBoomRealRadius(BOMB_TIMER_1)),

    BOOM_2(getBoomRealRadius(BOMB_TIMER_2)),

    BOOM_3(getBoomRealRadius(BOMB_TIMER_3)),

    BOOM_4(getBoomRealRadius(BOMB_TIMER_4)),

    BOOM_5(getBoomRealRadius(BOMB_TIMER_5)),

    BOOM_ALL(getAllBoomRealRadius());

    private final Function<Memory, Set<Cell>> func;

    ElementGroup(Function<Memory, Set<Cell>> func) {
        this.func = func;
    }

    private static Function<Memory, Set<Cell>> getBoomRealRadius(Elements element) {
        return m -> {
            return m.get(element).stream().flatMap(b -> {
                Player p = m.getLongMemory().bombOwner(b);
                if (p == null) {
                    log.warn("Bomb {} do not belong to any player!", b);
                    return b.getBoomAround(Player.DEFAULT_BOMB_RADIUS).stream();
                } else {
                    return b.getBoomAround(p.getBoomRadius()).stream();
                }
            }).collect(Collectors.toSet());
        };
    }

    private static Function<Memory, Set<Cell>> getAllBoomRealRadius() {
        return m -> {
            return m.get(ElementGroup.BOOMBS).stream().flatMap(b -> {
                Player p = m.getLongMemory().bombOwner(b);
                if (p == null) {
                    log.warn("Bomb {} do not belong to any player!", b);
                    return b.getBoomAround(Player.DEFAULT_BOMB_RADIUS).stream();
                } else {
                    return b.getBoomAround(p.getBoomRadius()).stream();
                }
            }).collect(Collectors.toSet());
        };
    }
}
