package com.codenjoy.dojo.bomberman.logic.asearch;

import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_BOMBERMAN;
import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_BOMB_BOMBERMAN;

import java.util.StringJoiner;
import java.util.concurrent.atomic.AtomicBoolean;

import org.apache.commons.collections4.queue.CircularFifoQueue;

import com.codenjoy.dojo.bomberman.client.Board;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.logic.asearch.strategy.ActStrategy;
import com.codenjoy.dojo.bomberman.logic.asearch.strategy.MoveStrategy;
import com.codenjoy.dojo.bomberman.memory.LongMemory;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.snapshot.Snapshot;
import com.codenjoy.dojo.client.Solver;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ASearchSolver implements Solver<Board> {

    private CircularFifoQueue<String> prevBoards = new CircularFifoQueue<>(3);
    private int cantMove = 0; //suicide on block

    private final LongMemory longMemory = new LongMemory();
    private final MoveStrategy move;
    private final ActStrategy act;
    private final AtomicBoolean running = new AtomicBoolean(false);

    public ASearchSolver(MoveStrategy move, ActStrategy act) {
        this.move = move;
        this.act = act;
    }

    @Override
    public String get(Board board) {
        running.set(true);
        addBlockingChecker();
        long start = System.currentTimeMillis();
        StringJoiner retval = new StringJoiner(",");
        Memory memory = new Memory(board, longMemory);
        if (!board.isMyBombermanDead()) {
            try {
                ASearch search = new ASearch(memory);
                Cell me = memory.getMe();
                ASearchRoute route = move.move(search, me); //
                if (route != null && route.getCommands() != null && !route.getCommands().isEmpty()) {
                    cantMove = 0;
                    // set the bomb (if possible) and go by the route
                    retval.add(route.getCommands().get(0).name());
                    if (act.act(search, me, route)) {
                        retval.add("ACT");
                    }
                } else {
                    cantMove++;
                    Snapshot.warn("\n" + board.boardAsString());
                    if (cantMove >= 10) {
                        log.error("Self destroy! Cant move {} turns", cantMove);
                        retval.add("ACT");
                    }
                }
            } catch (Exception e) {
                log.error("Exception ", e);
            }
        } else {
            if (!board.get(OTHER_BOMBERMAN, OTHER_BOMB_BOMBERMAN).isEmpty()) {
                // if i am dead and there is leave enemies, i loose..
                StringJoiner sb = new StringJoiner("\n", "\n", "");
                for (String prevBoard : prevBoards) {
                    sb.add(prevBoard);
                }
                sb.add(board.boardAsString());
                Snapshot.error(sb.toString());
            }
        }
        this.prevBoards.add(board.boardAsString());
        log.info(longMemory.getMe().toString());
        log.info("Returning [{}], move:{} act:{} time:{}, cantMove:{}", retval, move.getMoveTargets(),
                act.getActTargets(), System.currentTimeMillis() - start, cantMove);
        running.set(false);
        return retval.toString();
    }

    private void addBlockingChecker() {
        Thread current = Thread.currentThread();
        Thread checker = new Thread(() -> {
            while (running.get()) {
                try {
                    Thread.sleep(500L);
                    if (running.get()) {
                        log.error("Blocked! {}", current);
                        current.interrupt();
                    }
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
            }
        });
        checker.start();
    }
}
