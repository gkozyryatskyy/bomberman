package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import static com.codenjoy.dojo.bomberman.model.Elements.DEAD_MEAT_CHOPPER;
import static com.codenjoy.dojo.bomberman.model.Elements.MEAT_CHOPPER;
import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_BOMBERMAN;
import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_BOMB_BOMBERMAN;

import java.util.List;
import java.util.Set;
import java.util.function.Predicate;

import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRoute;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRouteFilters;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.model.Elements;

import lombok.Getter;

public class FarmerStrategy implements MoveStrategy, ActStrategy {

    //TODO
    @Getter
    private Set<Elements> moveTargets = Set.of();
    @Getter
    private Set<Elements> actTargets = Set.of();

    @Override
    public ASearchRoute move(ASearch search, Cell me) {
        this.actTargets = Set.of(OTHER_BOMBERMAN, OTHER_BOMB_BOMBERMAN, MEAT_CHOPPER, DEAD_MEAT_CHOPPER);
        List<Predicate<ASearchRoute>> filters = List.of(ASearchRouteFilters.hasWayBack(search),
                ASearchRouteFilters.noRouteThroughFuture2Blast(search),
                ASearchRouteFilters.noRouteThroughFuture3Blast(search),
                ASearchRouteFilters.noRouteThroughFuture5Blast(search));
        return StrategyUtils.goToPerks(search, me, 5, filters).orElseGet(() -> {
//                return goToBestDestroyableWallBoom(search, me, 4, filters).orElseGet(() -> {
                    return StrategyUtils.anyFree(search, me).orElse(null);
//                });
        });
    }



    @Override
    public boolean act(ASearch search, Cell me, ASearchRoute route) {
        return false;
    }

}
