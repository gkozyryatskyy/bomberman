package com.codenjoy.dojo.bomberman.logic.asearch.algo;

import java.util.HashMap;
import java.util.Map;

import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.ElementGroup;

public class CellScorer implements Scorer<Cell> {

    private final Map<Cell, Double> addScores = new HashMap<>();

    public CellScorer(Memory memory) {
        // cache booms around distance and add cost to this cells, for de prioritise routs through them
        memory.get(ElementGroup.BOOM_4).forEach(e -> addScores.put(e, 2d));
        memory.get(ElementGroup.BOOM_3).forEach(e -> addScores.put(e, 3d));
        memory.get(ElementGroup.BOOM_2).forEach(e -> addScores.put(e, 6d));
        // try not to go through BOMB_TIMER_5, because it is a RC(remote control) bomb
        memory.get(ElementGroup.BOOM_5).forEach(e -> addScores.put(e, 10d));
        // we do not go through booms from BOMB_TIMER_1 at all
    }

    @Override
    public double computeCost(Cell from, Cell to) {
        double retval = Math.abs(from.getX() - to.getX()) + Math.abs(from.getY() - to.getY());
        // increase cost from going through some cells
        Double add = addScores.get(from);
        if (add != null) {
            retval += add;
        }
        add = addScores.get(to);
        if (add != null) {
            retval += add;
        }
        return retval;
    }
}
