package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import java.util.Set;

import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRoute;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.model.Elements;

public interface MoveStrategy {

    Set<Elements> getMoveTargets();

    ASearchRoute move(ASearch search, Cell me);
}
