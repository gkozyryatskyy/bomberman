package com.codenjoy.dojo.bomberman.logic.asearch.algo;

public interface Scorer <T extends GraphNode> {

    double computeCost(T from, T to);
}
