package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import static com.codenjoy.dojo.bomberman.model.Elements.BOOM;
import static com.codenjoy.dojo.bomberman.model.Elements.DEAD_MEAT_CHOPPER;
import static com.codenjoy.dojo.bomberman.model.Elements.DESTROYABLE_WALL;
import static com.codenjoy.dojo.bomberman.model.Elements.MEAT_CHOPPER;
import static com.codenjoy.dojo.bomberman.model.Elements.NONE;
import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_BOMBERMAN;
import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_BOMB_BOMBERMAN;

import java.util.Collection;
import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.TreeSet;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRoute;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRouteFilters;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.memory.Player;
import com.codenjoy.dojo.bomberman.model.ElementGroup;
import com.codenjoy.dojo.bomberman.model.Elements;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BombersHunter implements MoveStrategy, ActStrategy {

    public static final Set<Elements> ANY_ACT_TARGET = new HashSet<>(
            List.of(OTHER_BOMBERMAN, OTHER_BOMB_BOMBERMAN, DESTROYABLE_WALL, MEAT_CHOPPER, DEAD_MEAT_CHOPPER));

    @Getter
    private Set<Elements> moveTargets = Set.of();
    @Getter
    private Set<Elements> actTargets = new HashSet<>();
    private int noActTurns = 1; // init with 1 for possible act in 1st turn
    private AtomicInteger rcBombTimer;

    @Override
    public ASearchRoute move(ASearch search, Cell me) {
        this.actTargets.addAll(List.of(OTHER_BOMBERMAN, OTHER_BOMB_BOMBERMAN, MEAT_CHOPPER, DEAD_MEAT_CHOPPER));
        List<Predicate<ASearchRoute>> filters = List.of(ASearchRouteFilters.hasWayBack(search),
                ASearchRouteFilters.noRouteThroughFuture2Blast(search),
                ASearchRouteFilters.noRouteThroughFuture3Blast(search),
                ASearchRouteFilters.noRouteThroughFuture5Blast(search));
        return goToPerks(search, me, 7, filters).orElseGet(() -> {
            //            return goToOtherBombers(search, me, filters).orElseGet(() -> {
            return goToBestDestroyableWallBoom(search, me, 5, filters).orElseGet(() -> {
                return goToDestroyableWall(search, me, 20, filters).orElseGet(() -> {
                    return goToNearestSafe(search, me, 4, List.of()).orElseGet(() -> {
                        return anyFree(search, me).orElse(null);
                    });
                });
            });
        }); //
    }

    public Optional<ASearchRoute> goToPerks(ASearch search, Cell me, int distance,
            List<Predicate<ASearchRoute>> filters) {
        this.moveTargets = Cell.PERKS;
        this.actTargets.add(DESTROYABLE_WALL); // boom walls while go to perk
        return StrategyUtils.goToPerks(search, me, distance, filters);
    }

    public Optional<ASearchRoute> goToBestDestroyableWallBoom(ASearch search, Cell meCell, int distance,
            List<Predicate<ASearchRoute>> filters) {
        this.moveTargets = Set.of(BOOM);
        this.actTargets.remove(DESTROYABLE_WALL); // we should act on this in the end of the turn
        Optional<ASearchRoute> retval;
        Set<Cell> wallBoom = new TreeSet<>((e1, e2) -> {
            int score = e1.getBoomScore().compareTo(e2.getBoomScore());
            if (score == 0) {
                return e1.getId().compareTo(e2.getId());
            } else {
                // using '-' for reverse order
                return -score;
            }
        });
        meCell.getRadius(distance).stream().filter(e -> e.getE().isCanGoThrough()).forEach(wallBoom::add);
        Queue<ASearchRoute> routes = search.findRoutes(meCell, () -> wallBoom);
        retval = ASearchRouteFilters.apply(routes, filters);
        return retval;
    }

    public Optional<ASearchRoute> goToDestroyableWall(ASearch search, Cell me, int distance,
            List<Predicate<ASearchRoute>> filters) {
        this.moveTargets = Set.of(DESTROYABLE_WALL);
        this.actTargets.add(DESTROYABLE_WALL);
        List<Cell> cells = search.getMemory()
                .get(DESTROYABLE_WALL)
                .stream()
                // filter distance
                .filter(e -> e.getX() <= me.getX() + distance && e.getX() >= me.getX() - distance)
                .filter(e -> e.getY() <= me.getY() + distance && e.getY() >= me.getY() - distance)
                .collect(Collectors.toList());
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> cells.stream()
                .flatMap(c -> c.getConnections().stream())
                .collect(Collectors.toCollection(TreeSet::new)));
        return ASearchRouteFilters.apply(routes, filters);
    }

    public Optional<ASearchRoute> goToNearestSafe(ASearch search, Cell me, int distance,
            List<Predicate<ASearchRoute>> filters) {
        this.moveTargets = Set.of(NONE);
        Set<Cell> futureBooms = search.getMemory().get(ElementGroup.BOOM_ALL);
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> {
            Set<Cell> safe = new TreeSet<>(me.getRadius(distance));
            // remove any future booms from safe`s
            safe.removeAll(futureBooms);
            return safe;
        });
        return ASearchRouteFilters.apply(routes, filters);
    }

    // last chance, try to go to any free cell
    public Optional<ASearchRoute> anyFree(ASearch search, Cell me) {
        this.moveTargets = Set.of();
        return StrategyUtils.anyFree(search, me);
    }

    public Optional<ASearchRoute> goToOtherBombers(ASearch search, Cell me, List<Predicate<ASearchRoute>> filters) {
        this.moveTargets = Set.of(OTHER_BOMBERMAN, OTHER_BOMB_BOMBERMAN);
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> {
            Collection<Cell> retval = search.getMemory()
                    .get(OTHER_BOMBERMAN, OTHER_BOMB_BOMBERMAN)
                    .stream()
                    .flatMap(c -> c.getBoomAround(1).stream())
                    .collect(Collectors.toCollection(TreeSet::new));
            retval.addAll(search.getMemory().get(Cell.PERKS));
            return retval;
        });
        return ASearchRouteFilters.apply(routes, filters);
    }

    public Optional<ASearchRoute> goToMeatChoppers(ASearch search, Cell me, List<Predicate<ASearchRoute>> filters) {
        this.moveTargets = Set.of(MEAT_CHOPPER);
        List<Cell> cells = search.getMemory().get(moveTargets);
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> cells.stream()
                .flatMap(c -> c.getBoomAround(2).stream())
                .collect(Collectors.toCollection(TreeSet::new)));
        return ASearchRouteFilters.apply(routes, filters);
    }

    @Override
    public boolean act(ASearch search, Cell me, ASearchRoute route) {
        return shouldAct(search, me, route);
    }

    public boolean shouldAct(ASearch search, Cell meCell, ASearchRoute route) {
        List<MoveCommand> moves = route.getCommands();
        MoveCommand moveCommand = moves.get(0);
        Cell futureMe = moveCommand.move(search.getMemory(), meCell);
        Cell actCell = futureMe;
        // hit DESTROYABLE_WALL if we have bomb count perk, and bomb was not set on previous turn
        if (search.getMemory().getLongMemory().getMe().getBoomCountTimer() > 2 && noActTurns > 0) {
            actTargets.add(DESTROYABLE_WALL);
            // if destination far away
        } else if (moves.size() > 5) {
            actTargets = ANY_ACT_TARGET;
        }
        // add DESTROYABLE_WALL target for finish route
        if (moves.size() == 1 && !moveTargets.isEmpty() && !moveTargets.contains(NONE)) {
            actTargets.add(DESTROYABLE_WALL);
        }
        // RC support
        Player me = search.getMemory().getLongMemory().getMe();
        boolean useDetonator = false;
        boolean rcBombTimerBoom = false;
        if (me.getRcBomb() != null) {
            actTargets.add(DESTROYABLE_WALL); // boom the wall fast, if we can
            useDetonator = true;
            actCell = me.getRcBomb();
            if (rcBombTimer != null) {
                rcBombTimer.decrementAndGet();
            } else {
                rcBombTimer = new AtomicInteger(3);
            }
            if (rcBombTimer.get() <= 0) {
                rcBombTimerBoom = true;
            }
        } else {
            rcBombTimer = null;
        }
        // check my boom cells according to my boom radius
        Set<Cell> visionCells = actCell.getBoomAround(me.getBoomRadius());
        Set<Cell> notMyBombs = actCell.getBoomAround(2);
        boolean nearMyBomb = me.getBombs().keySet().stream().anyMatch(notMyBombs::contains);
        // do not kill perks because of Zommbee DEAD_MEAT_CHOPPER
        boolean killingPerk = visionCells.stream().map(Cell::getE).anyMatch(Cell.PERKS::contains);
        // do not kill myself on RC bombs
        boolean killingMe = !me.isImmune() && visionCells.contains(futureMe);
        boolean retval = !nearMyBomb && // do not act few times in a row, or close to my other bomb
                !killingPerk && // do not kill perk
                !killingMe && // do not kill myself
                (rcBombTimerBoom || visionCells.stream().map(Cell::getE).anyMatch(e -> actTargets.contains(e)));
        if (me.getRcBomb() != null) {
            // TODO debug
            log.info("nearMyBomb:{}, killingPerk:{}, killingMe:{}, rcBombTimerBoom:{}", nearMyBomb, killingPerk,
                    killingMe, rcBombTimerBoom);
        }
        if (useDetonator && retval) {
            me.useDetonator();
            rcBombTimer = null;
        }
        if (retval) {
            noActTurns = 0;
        } else {
            noActTurns++;
        }
        return retval;
    }

}
