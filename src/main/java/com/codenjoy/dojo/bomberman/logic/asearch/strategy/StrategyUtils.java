package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.function.Predicate;
import java.util.stream.Collectors;

import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRoute;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRouteFilters;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.model.ElementGroup;
import com.codenjoy.dojo.bomberman.model.Elements;

public class StrategyUtils {

    public static Optional<ASearchRoute> anyFree(ASearch search, Cell me) {
        // if im on next turn boom
        Set<Cell> nextTurnBooms = search.getMemory().get(ElementGroup.BOOM_1);
        boolean boomDeath = new HashSet<>(search.getMemory().get(ElementGroup.BOOM_1)).contains(me);
        boolean chopperDeath = me.getConnections().stream().anyMatch(e -> Elements.MEAT_CHOPPER.equals(e.getE()));
        if (boomDeath) {
            // go to any isCanGoThrough cell
            return me.getConnections()
                    .stream()
                    .filter(e -> e.getE().isCanGoThrough())
                    .findAny()
                    .map(cell -> new ASearchRoute(me, cell, List.of(me, cell), 0));
        } else if (chopperDeath) {
            return me.getConnections()
                    .stream()
                    .filter(e -> e.getE().isCanGoThrough())
                    .filter(e -> !nextTurnBooms.contains(e))
                    .findAny()
                    .map(cell -> new ASearchRoute(me, cell, List.of(me, cell), 0));
        } else {
            return Optional.empty();
        }
    }

    public static Optional<ASearchRoute> goToPerks(ASearch search, Cell me, int distance,
            List<Predicate<ASearchRoute>> filters) {
        List<Cell> perks = search.getMemory()
                .get(Cell.ORDERED_PERKS)
                .stream()
                // filter distance
                .filter(e -> e.getX() <= me.getX() + distance && e.getX() >= me.getX() - distance)
                .filter(e -> e.getY() <= me.getY() + distance && e.getY() >= me.getY() - distance)
                .collect(Collectors.toList());
        Queue<ASearchRoute> routes = search.findRoutes(me, () -> perks);
        return ASearchRouteFilters.apply(routes, filters);
    }
}
