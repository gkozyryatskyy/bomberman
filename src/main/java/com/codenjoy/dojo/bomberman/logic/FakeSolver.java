package com.codenjoy.dojo.bomberman.logic;

import com.codenjoy.dojo.bomberman.client.Board;
import com.codenjoy.dojo.client.Solver;

public class FakeSolver implements Solver<Board>  {

    @Override
    public String get(Board board) {
        return "ACT,UP";
    }
}
