package com.codenjoy.dojo.bomberman.logic.asearch;

import java.util.ArrayList;
import java.util.List;

import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.RouteFinder;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.ToString;

@Getter
@ToString(exclude = "commands") // excluding commands for not init them if no need
@AllArgsConstructor
public class ASearchRoute implements Comparable<ASearchRoute> {

    private final Cell from;
    private final Cell to;
    private final List<Cell> route;
    private final double routeCost;
    private List<MoveCommand> commands;

    private Double finalCost;

    public Double getFinalCost() {
        if (finalCost == null) {
            if (to.getBoomScore() > 1) {
                finalCost = routeCost - (3 * to.getBoomScore());
            } else {
                finalCost = routeCost - to.getBoomScore();
            }
        }
        return finalCost;
    }

    public ASearchRoute(Cell from, Cell to, RouteFinder.Route<Cell> route) {
        this(from, to, route.getRoute(), route.getRouteScore());
    }

    public ASearchRoute(Cell from, Cell to, List<Cell> route, double routeCost) {
        this.from = from;
        this.to = to;
        this.route = route;
        this.routeCost = routeCost;
    }

    @Deprecated
    public boolean isEnded() {
        return from.equals(to);
    }

    // calculate commands just if we need them
    public List<MoveCommand> getCommands() {
        if (commands == null) {
            commands = routeToCommands(route);
        }
        return commands;
    }

    public static List<MoveCommand> routeToCommands(List<Cell> route) {
        if (route != null) {
            List<MoveCommand> retval = new ArrayList<>();
            Cell prev = null;
            for (Cell cell : route) {
                if (prev != null) {
                    retval.add(MoveCommand.getCommand(prev, cell));
                }
                prev = cell;
            }
            return retval;
        } else {
            return null;
        }
    }

    @Override
    public int compareTo(ASearchRoute other) {
        return Double.compare(getFinalCost(), other.getFinalCost());
    }
}
