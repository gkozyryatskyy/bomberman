package com.codenjoy.dojo.bomberman.logic.asearch.algo;

import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_BLAST_RADIUS_INCREASE;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_BOMBERMAN;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_COUNT_INCREASE;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_IMMUNE;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_REMOTE_CONTROL;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_1;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_2;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_3;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_4;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_TIMER_5;
import static com.codenjoy.dojo.bomberman.model.Elements.OTHER_BOMB_BOMBERMAN;

import java.util.Arrays;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import java.util.function.Function;

import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.Elements;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.AllArgsConstructor;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.ToString;

@Getter
@AllArgsConstructor
@ToString(includeFieldNames = false, of = { "id", "e" })
@EqualsAndHashCode(of = { "id" })
public class Cell implements GraphNode, Comparable<Cell> {

    public static final Set<Elements> BOMBS = Set.of(BOMB_TIMER_1, BOMB_TIMER_2, BOMB_TIMER_3, BOMB_TIMER_4,
            BOMB_TIMER_5, BOMB_BOMBERMAN, OTHER_BOMB_BOMBERMAN);

    public static final List<Elements> ORDERED_PERKS = List.of(BOMB_IMMUNE, BOMB_BLAST_RADIUS_INCREASE,
            BOMB_REMOTE_CONTROL, BOMB_COUNT_INCREASE);
    public static final Set<Elements> PERKS = new HashSet<>(ORDERED_PERKS);

    private final int x;
    private final int y;
    private final Elements e;
    private final Memory memory;
    private final String id;

    private Long score;
    private Long boomScore;

    public Cell(int x, int y, Elements e, Memory memory) {
        this.x = x;
        this.y = y;
        this.e = e;
        this.memory = memory;
        this.id = x + ":" + y;
    }

    public boolean isBomb() {
        return BOMBS.contains(e);
    }

    public Long getScore() {
        if (score == null) {
            this.score = getConnections().stream().filter(e -> e.getE().isCanGoThrough()).count();
        }
        return score;
    }

    public Long getBoomScore() {
        // calculate distraction
        if (boomScore == null) {
            this.boomScore = getBoomAround(memory.getLongMemory().getMe().getBoomRadius())
                    .stream()
                    .filter(e -> Elements.DESTROYABLE_WALL.equals(e.getE()))
                    .count();
        }
        return boomScore;
    }

    @Override
    public Set<Cell> getConnections() {
        return getAround(1);
    }

    public Set<Cell> getAround(int distance) {
        Set<Cell> retval = new HashSet<>();
        // order from far to near
        for (int i = distance; i > 0; i--) {
            retval.addAll(Arrays.asList(memory.get(x + i, y), memory.get(x - i, y), memory.get(x, y + i),
                    memory.get(x, y - i)));
        }
        return retval;
    }

    public Set<Cell> getRadius(int radius) {
        Set<Cell> retval = new HashSet<>();
        // order from far to near
        for (int i = x - radius; i <= x + radius; i++) {
            for (int j = y - radius; j <= y + radius; j++) {
                if (x != i || y != j) { // exclude me
                    retval.add(memory.get(i, j));
                }
            }
        }
        return retval;
    }

    public Set<Cell> getBoomAround(int distance) {
        Set<Cell> retval = new HashSet<>();
        iterateDirection(retval, memory, distance, MoveCommand.UP, Elements::isCanBoomThrough);
        iterateDirection(retval, memory, distance, MoveCommand.DOWN, Elements::isCanBoomThrough);
        iterateDirection(retval, memory, distance, MoveCommand.RIGHT, Elements::isCanBoomThrough);
        iterateDirection(retval, memory, distance, MoveCommand.LEFT, Elements::isCanBoomThrough);
        return retval;
    }

    private void iterateDirection(Set<Cell> retval, Memory mem, int distance, MoveCommand direction,
            Function<Elements, Boolean> filter) {
        if (distance > 0) {
            Cell cur = this;
            do {
                cur = direction.move(mem, cur);
                retval.add(cur);
                distance--;
            } while (filter.apply(cur.getE()) && distance > 0);
        }
    }

    @Override
    public int compareTo(Cell o) {
        int retval = getScore().compareTo(o.getScore());
        if (retval == 0) {
            return id.compareTo(o.id);
        } else {
            // using '-' for reverse order
            return -retval;
        }
    }
}
