package com.codenjoy.dojo.bomberman.logic.asearch;

import java.util.HashSet;
import java.util.List;
import java.util.Optional;
import java.util.Queue;
import java.util.Set;
import java.util.function.Predicate;

import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.model.ElementGroup;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ASearchRouteFilters {

    public static Optional<ASearchRoute> apply(Queue<ASearchRoute> routes, List<Predicate<ASearchRoute>> filters) {
        return routes.stream().filter(filters.stream().reduce(x -> true, Predicate::and)).findAny();
    }

    public static Predicate<ASearchRoute> hasWayBack(ASearch search) {
        return (route) -> {
            // check if we have a way back, to any close cell not from the route
            Queue<ASearchRoute> back = search.findRoutes(route.getTo(), () -> {
                Set<Cell> backCells = new HashSet<>(route.getFrom().getConnections());
                // remove any rout cell from back cells
                backCells.removeAll(route.getRoute());
                return backCells;
            });
            if (!back.isEmpty()) {
                log.debug(route.toString());
                log.debug("way back:{}", back);
                return true;
            } else {
                return false;
            }
        };
    }

    public static Predicate<ASearchRoute> noRouteThroughFuture2Blast(ASearch search) {
        return (route) -> {
            if (!search.getMemory().getLongMemory().getMe().isImmune() && route.getRoute().size() > 2) {
                return !new HashSet<>(search.getMemory().get(ElementGroup.BOOM_2)).contains(route.getRoute().get(2));
            } else {
                return true;
            }
        };
    }

    public static Predicate<ASearchRoute> noRouteThroughFuture3Blast(ASearch search) {
        return (route) -> {
            if (!search.getMemory().getLongMemory().getMe().isImmune() && route.getRoute().size() > 3) {
                return !new HashSet<>(search.getMemory().get(ElementGroup.BOOM_3)).contains(route.getRoute().get(3));
            } else {
                return true;
            }
        };
    }

    public static Predicate<ASearchRoute> noRouteThroughFuture5Blast(ASearch search) {
        return (route) -> {
            if (!search.getMemory().getLongMemory().getMe().isImmune() && route.getRoute().size() > 1) {
                return !new HashSet<>(search.getMemory().get(ElementGroup.BOOM_5)).contains(route.getRoute().get(1));
            } else {
                return true;
            }
        };
    }
}
