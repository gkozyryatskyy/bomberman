package com.codenjoy.dojo.bomberman.logic.asearch;

import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_BLAST_RADIUS_INCREASE;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_COUNT_INCREASE;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_IMMUNE;
import static com.codenjoy.dojo.bomberman.model.Elements.BOMB_REMOTE_CONTROL;
import static com.codenjoy.dojo.bomberman.model.Elements.BOOM;
import static com.codenjoy.dojo.bomberman.model.Elements.DEAD_MEAT_CHOPPER;
import static com.codenjoy.dojo.bomberman.model.Elements.MEAT_CHOPPER;
import static com.codenjoy.dojo.bomberman.model.Elements.NONE;

import java.util.Collection;
import java.util.HashMap;
import java.util.Map;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;
import java.util.function.Supplier;

import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.CellScorer;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Graph;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.RouteFinder;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.ElementGroup;
import com.codenjoy.dojo.bomberman.model.Elements;

import lombok.Getter;
import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ASearch {

    // this set is different from Elements.canGoThrough, because even if we can go through, do not mean that we want to
    public static final Set<Elements> ACCESSIBLE_ELEMENTS = Set.of( // go through
            NONE, // empty cell is ok
            BOOM, // boom, because it will become empty cell next turn
            Elements.BOMBERMAN, // myself is ok? myself block helps us not to go to the traps
            BOMB_COUNT_INCREASE, // BOMB_COUNT_INCREASE is ok, because it is not changing behavior a lot
            BOMB_IMMUNE, // BOMB_IMMUNE is ok, because it is just positive perk
            BOMB_BLAST_RADIUS_INCREASE, // BOMB_BLAST_RADIUS_INCREASE is ok, because it is supported with LongMemory
            BOMB_REMOTE_CONTROL); //BOMB_REMOTE_CONTROL is ok, because it is supported with LongMemory

    @Getter
    private final Memory memory;
    private final Graph<Cell> availableBoard;
    private final RouteFinder<Cell> routeFinder;

    public ASearch(Memory memory) {
        this.memory = memory;
        Map<String, Cell> accessibleCells = new HashMap<>();
        memory.get(ACCESSIBLE_ELEMENTS).forEach(c -> accessibleCells.put(c.getId(), c));
        // --------------------- remove future booms from 1 sec bombs ---------------------
        // get all bombs that will blow up next turn
        memory.get(ElementGroup.BOOM_1).forEach(e -> accessibleCells.remove(e.getId()));
        // --------------------- remove possible meatChoppers ways ---------------------
        // get all meatChoppers
        // TODO DEAD_MEAT_CHOPPER stub for zombee DEAD_MEAT_CHOPPER
        memory.get(MEAT_CHOPPER, DEAD_MEAT_CHOPPER).stream()
                // get all around meatChoppers cells and remove them from possible moves
                .flatMap(e -> e.getAround(1).stream()).forEach(e -> accessibleCells.remove(e.getId()));
        // --------------------- remove possible booms from RC(remote controlled bombs) ---------------------
        this.availableBoard = new Graph<>(accessibleCells);
        this.routeFinder = new RouteFinder<>(availableBoard, new CellScorer(memory), new CellScorer(memory));
    }

    protected ASearchRoute findRoute(Cell from, Cell to) {
        if (from.equals(to)) {
            return null;
        } else {
            RouteFinder.Route<Cell> route = routeFinder.findRoute(from, to);
            // init ASearchRoute for logging, even if it has null route
            if (route == null) {
                log.debug("Route from {} to {} is null", from, to);
                return null;
            } else {
                ASearchRoute retval = new ASearchRoute(from, to, route);
                log.debug(retval.toString());
                return retval;
            }
        }
    }

    public Queue<ASearchRoute> findRoutes(Cell from, Supplier<Collection<Cell>> routesFunc) {
        Queue<ASearchRoute> retval = new PriorityQueue<>();
        for (Cell goTo : routesFunc.get()) {
            // check that this node in the accessible board
            if (availableBoard.getNode(goTo.getId()) != null) {
                ASearchRoute route = findRoute(from, goTo);
                if (route != null) {
                    retval.add(route);
                }
            }
        }
        return retval;
    }
}
