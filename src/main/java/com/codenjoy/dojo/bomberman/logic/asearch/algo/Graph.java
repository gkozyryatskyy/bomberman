package com.codenjoy.dojo.bomberman.logic.asearch.algo;

import java.util.Map;
import java.util.Objects;
import java.util.Set;
import java.util.stream.Collectors;

import lombok.AllArgsConstructor;

@AllArgsConstructor
public class Graph<T extends GraphNode> {

    private final Map<String, T> nodes;

    public T getNode(String id) {
        return nodes.get(id);
    }

    public Set<T> getConnections(T node) {
        return node.getConnections()
                .stream()
                .map(e -> getNode(e.getId()))
                .filter(Objects::nonNull)
                .collect(Collectors.toSet());
    }
}
