package com.codenjoy.dojo.bomberman.logic.asearch.algo;

import java.util.Set;

public interface GraphNode {

    String getId();

    Set<Cell> getConnections();

}
