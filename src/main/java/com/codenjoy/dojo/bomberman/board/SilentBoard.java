package com.codenjoy.dojo.bomberman.board;

import com.codenjoy.dojo.bomberman.client.Board;

public class SilentBoard extends Board {

    @Override
    public String toString() {
        return "Layers " + countLayers() + "[" + getField().length + ":" + getField()[0].length + "]";
    }
}
