package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import java.util.List;

import org.junit.Assert;
import org.junit.Ignore;
import org.junit.Test;

import com.codenjoy.dojo.bomberman.BaseTest;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BomberHunterTrapsTest {

    private final BombersHunter strategy = new BombersHunter();

    @Test
    public void doNotGoToTheTrapTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼     #♥☼" //
                + "☼ ☼ ☼ ☼#☼" //
                + "☼   # # ☼" //
                + "☼ ☼#☼ ☼ ☼" //
                + "☼ #  ☻# ☼" //
                + "☼ ☼#☼&☼ ☼" //
                + "☼       ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        Assert.assertNotNull(commands);
        log.info(commands.toString());
        // check do not go into the dead end
        Assert.assertNotEquals(List.of(MoveCommand.LEFT), commands);
    }

    @Test
    public void doNotGoToTheTrapTest2() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼  ☻    ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼  ♥    ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        log.info(commands.toString());
        Assert.assertNotNull(commands);
        // check do not stay on the boom
        Assert.assertNotEquals(MoveCommand.DOWN, commands.get(0));
    }

    @Test
    public void doNotGoToTheTrapTest3() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼#☼#☼#☼ ☼" //
                + "☼# ☻2   ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼&      ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        log.info(commands.toString());
        Assert.assertNotNull(commands);
        // check do not stay on the boom
        Assert.assertNotEquals(MoveCommand.LEFT, commands.get(0));
    }

    @Test
    public void doNotGoToTheTrapTest4() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼#☼#☼#☼ ☼" //
                + "☼  ☺4   ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼#      ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        log.info(commands.toString());
        Assert.assertNotNull(commands);
        // check do not stay on the boom
        Assert.assertNotEquals(MoveCommand.LEFT, commands.get(0));
    }

    @Test
    public void doNotGoToTheTrapTest5() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼  #  # ☼" //
                + "☼ ☼ ☼i☼ ☼" //
                + "☼    ☻  ☼" //
                + "☼#☼#☼#☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼#      ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        log.info(commands.toString());
        Assert.assertNotNull(commands);
        // check do not stay on the boom
        Assert.assertNotEquals(MoveCommand.UP, commands.get(0));
    }

    //TODO fix this somehow
    @Test
    @Ignore
    public void doNotGoToTheTrapTest6() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼     # ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼  ☺ 2# ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼  2    ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼#      ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        log.info(commands.toString());
        Assert.assertNotNull(commands);
        // check do not stay on the boom
        Assert.assertNotEquals(MoveCommand.RIGHT, commands.get(0));
        Assert.assertNotEquals(MoveCommand.DOWN, commands.get(0));
    }

    @Test // test for triple cell dead end, do not go there
    public void doNotGoToTheTrapRightTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼  ☻   ♥☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        Assert.assertNotNull(commands);
        log.info(commands.toString());
        // check do not go into the dead end
        Assert.assertNotEquals(MoveCommand.RIGHT, commands.get(0));
    }

    @Test // test for triple cell dead end, do not go there
    public void doNotGoToTheTrapLeftTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼#☼#☼ ☼ ☼" //
                + "☼♥   ☻  ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        Assert.assertNotNull(commands);
        log.info(commands.toString());
        // check do not go into the dead end
        Assert.assertNotEquals(MoveCommand.LEFT, commands.get(0));
    }

    @Test
    public void goOutFromTheTrapWithABombTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ☺  3☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        Assert.assertNotNull(commands);
        log.info(commands.toString());
        // check do not go into the dead end
        Assert.assertEquals(MoveCommand.LEFT, commands.get(0));
    }

    @Test
    public void goOutFromTheTrapWithMyBombTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ☻   ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        log.info(commands.toString());
        Assert.assertNotNull(commands);
        // check do not go into the dead end
        Assert.assertEquals(MoveCommand.LEFT, commands.get(0));
    }

}
