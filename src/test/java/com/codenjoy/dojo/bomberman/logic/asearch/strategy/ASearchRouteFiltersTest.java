package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import com.codenjoy.dojo.bomberman.BaseTest;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRoute;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRouteFilters;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class ASearchRouteFiltersTest {

    private final BombersHunter strategy = new BombersHunter();

    @Test
    public void filterPriorityTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼☺☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼  ♥    ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        Optional<ASearchRoute> route = strategy.goToOtherBombers(search, me,
                List.of(ASearchRouteFilters.hasWayBack(search)));
        Assert.assertTrue(route.isPresent());
        List<MoveCommand> res = route.get().getCommands();
        log.info(res.toString());
        Assert.assertEquals(MoveCommand.DOWN, res.get(0));
    }

    @Test // test for single cell dead end, do not go there
    public void filterHasWayBackSingleCellTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼#☼" //
                + "☼    ☻ ♥☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        Optional<ASearchRoute> route = strategy.goToOtherBombers(search, me,
                List.of(ASearchRouteFilters.hasWayBack(search)));
        Assert.assertTrue(route.isPresent());
        List<MoveCommand> res = route.get().getCommands();
        log.info(res.toString());
        // check do not go into the dead end
        Assert.assertNotEquals(List.of(MoveCommand.RIGHT), res);
    }

    @Test // test for triple cell dead end, do not go there
    public void filterHasWayBackTripleCellTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼  ☻   ♥☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        Optional<ASearchRoute> route = strategy.goToOtherBombers(search, me,
                List.of(ASearchRouteFilters.hasWayBack(search)));
        Assert.assertTrue(route.isPresent());
        List<MoveCommand> res = route.get().getCommands();
        log.info(res.toString());
        // check do not go into the dead end
        Assert.assertNotEquals(List.of(MoveCommand.RIGHT, MoveCommand.RIGHT, MoveCommand.RIGHT), res);
    }

    @Test // test for triple cell dead end, do not go there
    public void noRouteThroughFutureBlast2Test() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼2☼ ☼#☼" //
                + "☼ #     ☼" //
                + "☼ ☼ ☼#☼ ☼" //
                + "☼ #☺    ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        Optional<ASearchRoute> route = strategy.goToOtherBombers(search, me,
                List.of(ASearchRouteFilters.noRouteThroughFuture2Blast(search)));
        Assert.assertTrue(route.isPresent());
        List<MoveCommand> res = route.get().getCommands();
        log.info(res.toString());
        // do not plan to go though blast in 2 turns
        Assert.assertNotEquals(List.of(MoveCommand.UP, MoveCommand.UP), res.subList(0, 2));
    }

    @Test // test for triple cell dead end, do not go there
    public void noRouteThroughFutureBlast3Test() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼3☼ ☼ ☼" //
                + "☼ #     ☼" //
                + "☼ ☼ ☼ ☼#☼" //
                + "☼ # #   ☼" //
                + "☼ ☼☺☼ ☼ ☼" //
                + "☼ #     ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        Optional<ASearchRoute> route = strategy.goToOtherBombers(search, me,
                List.of(ASearchRouteFilters.noRouteThroughFuture3Blast(search)));
        Assert.assertTrue(route.isPresent());
        List<MoveCommand> res = route.get().getCommands();
        log.info(res.toString());
        // do not plan to go though blast in 2 turns
        Assert.assertNotEquals(List.of(MoveCommand.UP, MoveCommand.UP, MoveCommand.UP), res.subList(0, 3));
    }
}
