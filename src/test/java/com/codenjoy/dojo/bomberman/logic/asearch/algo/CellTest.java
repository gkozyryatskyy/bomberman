package com.codenjoy.dojo.bomberman.logic.asearch.algo;

import java.util.HashSet;
import java.util.Set;

import org.junit.Assert;
import org.junit.Test;

import com.codenjoy.dojo.bomberman.BaseTest;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.Elements;

public class CellTest {

    private final Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
            + "☼     #♥☼" //
            + "☼ ☼ ☼ ☼#☼" //
            + "☼   # # ☼" //
            + "☼ ☼#☼ ☼ ☼" //
            + "☼ #  ☻# ☼" //
            + "☼ ☼#☼&☼ ☼" //
            + "☼       ☼" //
            + "☼☼☼☼☼☼☼☼☼");

    @Test
    public void getConnectionsTest() {
        Cell cell = board.get(1, 1);
        Set<Cell> expected = Set.of(new Cell(0, 1, Elements.WALL, board), new Cell(2, 1, Elements.NONE, board),
                new Cell(1, 0, Elements.WALL, board), new Cell(1, 2, Elements.NONE, board));
        Assert.assertEquals(expected, new HashSet<>(cell.getConnections()));
    }

    @Test
    public void getAround1() {
        Cell cell = board.get(1, 1);
        Set<Cell> expected = Set.of(new Cell(0, 1, Elements.WALL, board), new Cell(2, 1, Elements.NONE, board),
                new Cell(1, 0, Elements.WALL, board), new Cell(1, 2, Elements.NONE, board));
        Assert.assertEquals(expected, new HashSet<>(cell.getAround(1)));
    }

    @Test
    public void getAround2() {
        Cell cell = board.get(5, 5);
        Set<Cell> expected = Set.of(new Cell(3, 5, Elements.NONE, board),
                new Cell(4, 5, Elements.DESTROYABLE_WALL, board), new Cell(6, 5, Elements.DESTROYABLE_WALL, board),
                new Cell(7, 5, Elements.NONE, board), new Cell(5, 3, Elements.BOMB_BOMBERMAN, board),
                new Cell(5, 4, Elements.NONE, board), new Cell(5, 6, Elements.NONE, board),
                new Cell(5, 7, Elements.NONE, board));
        Assert.assertEquals(expected, new HashSet<>(cell.getAround(2)));
    }

    @Test
    public void getAround3() {
        Cell cell = board.get(5, 5);
        Set<Cell> expected = Set.of(new Cell(2, 5, Elements.NONE, board), new Cell(3, 5, Elements.NONE, board),
                new Cell(4, 5, Elements.DESTROYABLE_WALL, board), new Cell(6, 5, Elements.DESTROYABLE_WALL, board),
                new Cell(7, 5, Elements.NONE, board), new Cell(8, 5, Elements.WALL, board),
                new Cell(5, 2, Elements.MEAT_CHOPPER, board), new Cell(5, 3, Elements.BOMB_BOMBERMAN, board),
                new Cell(5, 4, Elements.NONE, board), new Cell(5, 6, Elements.NONE, board),
                new Cell(5, 7, Elements.NONE, board), new Cell(5, 8, Elements.WALL, board));
        Assert.assertEquals(expected, new HashSet<>(cell.getAround(3)));
    }

    @Test
    public void getBoomAround() {
        Cell me = board.getMe(); // 5, 3
        Set<Cell> expected = Set.of(new Cell(5, 4, Elements.NONE, board), new Cell(5, 5, Elements.NONE, board),
                new Cell(5, 6, Elements.NONE, board), new Cell(5, 2, Elements.MEAT_CHOPPER, board),
                new Cell(5, 1, Elements.NONE, board), new Cell(5, 0, Elements.WALL, board),
                new Cell(6, 3, Elements.DESTROYABLE_WALL, board), new Cell(4, 3, Elements.NONE, board),
                new Cell(3, 3, Elements.NONE, board), new Cell(2, 3, Elements.DESTROYABLE_WALL, board));
        Assert.assertEquals(expected, new HashSet<>(me.getBoomAround(3)));
    }
}
