package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.codenjoy.dojo.bomberman.BaseTest;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRoute;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.memory.LongMemory;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.memory.Player;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BombersHunterRCTest {

    private final BombersHunter strategy = new BombersHunter();

    @Test
    public void rcBombTest() {
        LongMemory mem = new LongMemory();
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼♥☼#☼#☼" //
                + "☼ r☺    ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(MoveCommand.LEFT, strategy.move(new ASearch(board), board.getMe()).getCommands().get(0));
        board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼♥☼#☼#☼" //
                + "☼ ☺     ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(Player.DETONATORS_INCREASE, mem.getMe().getDetonators());
        ASearch search = new ASearch(board);
        // fake route RIGHT just to simulate situation
        Cell to = board.get(3, 1);
        ASearchRoute fakeRoute = new ASearchRoute(board.getMe(), to, List.of(board.getMe(), to), 0);
        Assert.assertTrue(strategy.act(search, board.getMe(), fakeRoute));
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼♥☼#☼#☼" //
                + "☼  ☻    ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals("3:1", mem.getMe().getRcBomb().getId());
        Assert.assertEquals(4, mem.getMe().getRcBombTimer().get());
        board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼♥☼#☼#☼" //
                + "☼  5☺   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        search = new ASearch(board);
        ASearchRoute route = strategy.move(search, board.getMe());
        Assert.assertEquals(MoveCommand.RIGHT, route.getCommands().get(0));
        // do not blow myself
        Assert.assertFalse(strategy.act(search, board.getMe(), route));
        Assert.assertEquals(3, mem.getMe().getRcBombTimer().get());
        board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼♥☼#☼#☼" //
                + "☼  5  ☺ ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        search = new ASearch(board);
        route = strategy.move(search, board.getMe());
        Assert.assertEquals(MoveCommand.RIGHT, route.getCommands().get(0));
        // we can detonate, because we leave boom radius in 1 turn
        Assert.assertTrue(strategy.act(search, board.getMe(), route));
        Assert.assertEquals(Player.DETONATORS_INCREASE - 1, mem.getMe().getDetonators());
        Assert.assertEquals(2, mem.getMe().getRcBombTimer().get());
    }

}
