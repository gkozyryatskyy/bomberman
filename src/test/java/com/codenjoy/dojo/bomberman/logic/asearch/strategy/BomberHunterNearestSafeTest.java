package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import java.util.List;
import java.util.Optional;

import org.junit.Assert;
import org.junit.Test;

import com.codenjoy.dojo.bomberman.BaseTest;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRoute;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BomberHunterNearestSafeTest {

    private final BombersHunter strategy = new BombersHunter();

    @Test
    public void goToNearestSafeTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼1☼ ☼ ☼" //
                + "☼  ☻♥   ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼       ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Optional<ASearchRoute> route = strategy.goToNearestSafe(search, board.getMe(), 4, List.of());
        Assert.assertTrue(route.isPresent());
        log.info(route.get().getCommands().toString());
        Assert.assertEquals(MoveCommand.LEFT, route.get().getCommands().get(0));
    }

    @Test
    public void goToNearestSafeTest2() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      &☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼    1 ☻☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼      ♥☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Optional<ASearchRoute> route = strategy.goToNearestSafe(search, board.getMe(), 4, List.of());
        Assert.assertTrue(route.isPresent());
        log.info(route.get().getCommands().toString());
        Assert.assertEquals(List.of(MoveCommand.DOWN, MoveCommand.DOWN, MoveCommand.LEFT), route.get().getCommands());
    }

    @Test
    public void goToNearestSafeTest3() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼   2☺  ☼" //
                + "☼ ☼ ☼#☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Optional<ASearchRoute> route = strategy.goToNearestSafe(search, board.getMe(), 4, List.of());
        Assert.assertTrue(route.isPresent());
        log.info(route.get().getCommands().toString());
        Assert.assertEquals(List.of(MoveCommand.RIGHT, MoveCommand.RIGHT, MoveCommand.DOWN),
                route.get().getCommands().subList(0, 3));
    }
}
