package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.codenjoy.dojo.bomberman.BaseTest;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BomberHunterWallBoomTest {

    private final BombersHunter strategy = new BombersHunter();

    @Test
    public void wallBoomTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼     # ☼" //
                + "☼ ☼ ☼ ☼#☼" //
                + "☼   # # ☼" //
                + "☼ ☼ ☼ ☼#☼" //
                + "☼     ☺ ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        List<MoveCommand> commands = strategy.move(search, board.getMe()).getCommands();
        Assert.assertNotNull(commands);
        log.info(commands.toString());
        // check do not go into the dead end
        Assert.assertEquals(List.of(MoveCommand.LEFT, MoveCommand.UP, MoveCommand.UP), commands);
    }
}
