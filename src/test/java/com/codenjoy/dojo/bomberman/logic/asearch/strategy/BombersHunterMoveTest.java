package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.codenjoy.dojo.bomberman.BaseTest;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRoute;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BombersHunterMoveTest {

    private final BombersHunter strategy = new BombersHunter();

    @Test
    public void moveTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼"
                + "☼  ## # # #   #       ☼"
                + "☼ ☼#☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼"
                + "☼      #  #   #     # ☼"
                + "☼ ☼ ☼#☼ ☼ ☼ ☼#☼&☼ ☼ ☼#☼"
                + "☼&   #   &         #  ☼"
                + "☼ ☼ ☼ ☼ ☼ ☼ ☼#☼#☼ ☼ ☼ ☼"
                + "☼             #    3  ☼"
                + "☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼☺☼ ☼"
                + "☼                     ☼"
                + "☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼ ☼ ☼"
                + "☼ #      # #  #  #    ☼"
                + "☼#☼#☼ ☼ ☼ ☼ ☼#☼ ☼ ☼ ☼ ☼"
                + "☼ ## 1 #  ♥    #     #☼"
                + "☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼"
                + "☼  ♥  #   #       i # ☼"
                + "☼ ☼ ☼#☼#☼#☼ ☼ ☼ ☼ ☼ ☼ ☼"
                + "☼#     #    &    #    ☼"
                + "☼ ☼ ☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼"
                + "☼        ♥            ☼"
                + "☼#☼ ☼#☼#☼#☼ ☼ ☼ ☼ ☼ ☼ ☼"
                + "☼ #   #  #      &     ☼"
                + "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        ASearchRoute route = strategy.move(search, me);
        Assert.assertNotNull(route);
        log.info(route.getCommands().toString());
        // check do not stay on the boom
        Assert.assertEquals(MoveCommand.DOWN, route.getCommands().get(0));
    }

    @Test
    public void moveTest2() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼"
                + "☼#           &     ☻  ☼"
                + "☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼#☼#☼#☼"
                + "☼       ## #     &    ☼"
                + "☼ ☼ ☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼"
                + "☼     ##         & ♥ #☼"
                + "☼ ☼ ☼#☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼"
                + "☼ # # ♥   #    #      ☼"
                + "☼ ☼#☼ ☼#☼#☼ ☼ ☼ ☼ ☼ ☼ ☼"
                + "☼♥         &  #   #1  ☼"
                + "☼ ☼ ☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼"
                + "☼2 #  #     #         ☼"
                + "☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼ ☼#☼"
                + "☼ #& #    #  #    #  #☼"
                + "☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼"
                + "☼          #         #☼"
                + "☼ ☼ ☼#☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼"
                + "☼#                    ☼"
                + "☼ ☼#☼ ☼ ☼ ☼ ☼ ☼#☼ ☼ ☼ ☼"
                + "☼# ♥## #              ☼"
                + "☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼ ☼#☼ ☼"
                + "☼    1         # #  # ☼"
                + "☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        ASearchRoute route = strategy.move(search, me);
        Assert.assertNotNull(route);
        log.info(route.getCommands().toString());
        // check do not stay on the boom
        Assert.assertEquals(MoveCommand.LEFT, route.getCommands().get(0));
    }

    @Test
    public void movePerkPriorityTest() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼  i +  ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼r  ☺  c☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        ASearchRoute route = strategy.move(search, board.getMe());
        Assert.assertNotNull(route);
        log.info(route.getCommands().toString());
        // check do not stay on the boom
        Assert.assertEquals(List.of(MoveCommand.LEFT, MoveCommand.UP, MoveCommand.UP), route.getCommands());
    }

    @Test
    public void moveTest3() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼&☼ ☼ ☼ ☼" //
                + "☼ ♥     ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼☻&     ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        ASearchRoute route = strategy.move(search, board.getMe());
        Assert.assertNotNull(route);
        log.info(route.getCommands().toString());
        // check do not stay on the boom
        Assert.assertEquals(MoveCommand.UP, route.getCommands().get(0));
    }

    @Test
    public void movePerkPriorityTest2() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼  i    ☼" //
                + "☼ ☼ ☼+☼ ☼" //
                + "☼r  ☺  c☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        ASearchRoute route = strategy.move(search, board.getMe());
        Assert.assertNotNull(route);
        log.info(route.getCommands().toString());
        // check do not stay on the boom
        Assert.assertEquals(List.of(MoveCommand.RIGHT, MoveCommand.UP), route.getCommands());
    }

}
