package com.codenjoy.dojo.bomberman.logic.asearch.strategy;

import java.util.List;

import org.junit.Assert;
import org.junit.Test;

import com.codenjoy.dojo.bomberman.BaseTest;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearch;
import com.codenjoy.dojo.bomberman.logic.asearch.ASearchRoute;
import com.codenjoy.dojo.bomberman.logic.asearch.algo.Cell;
import com.codenjoy.dojo.bomberman.memory.Memory;
import com.codenjoy.dojo.bomberman.model.MoveCommand;

import lombok.extern.slf4j.Slf4j;

@Slf4j
public class BomberHunterBoomTest {

    private final BombersHunter strategy = new BombersHunter();

    @Test
    public void doNotGoToTheBoom() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼1☼#☼#☼" //
                + "☼   ☺#  ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        ASearchRoute route = strategy.move(search, me);
        Assert.assertNull(route);
    }

    @Test
    public void doNotStayOnTheBoom() {
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼      ♥☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼1☼ ☼ ☼ ☼" //
                + "☼2      ☼" //
                + "☼3☼♥☼#☼#☼" //
                + "☼☺      ☼" //
                + "☼☼☼☼☼☼☼☼☼");
        ASearch search = new ASearch(board);
        Cell me = board.getMe();
        List<MoveCommand> commands = strategy.move(search, me).getCommands();
        Assert.assertNotNull(commands);
        log.info(commands.toString());
        // check do not stay on the boom
        Assert.assertEquals(MoveCommand.RIGHT, commands.get(0));
    }
}
