package com.codenjoy.dojo.bomberman;

import com.codenjoy.dojo.bomberman.client.Board;
import com.codenjoy.dojo.bomberman.memory.LongMemory;
import com.codenjoy.dojo.bomberman.memory.Memory;

public class BaseTest {

    public static Memory board(String b) {
        return board(b, new LongMemory());
    }

    public static Memory board(String b, LongMemory mem) {
        return new Memory((Board) new Board().forString(b), mem);
    }
}
