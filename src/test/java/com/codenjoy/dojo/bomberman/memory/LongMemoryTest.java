package com.codenjoy.dojo.bomberman.memory;

import java.util.concurrent.atomic.AtomicInteger;

import org.junit.Assert;
import org.junit.Test;

import com.codenjoy.dojo.bomberman.BaseTest;

public class LongMemoryTest {

    @Test
    public void hiddenBombsTest() {
        LongMemory mem = new LongMemory();
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ♥   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(0, mem.getHiddenBombs().size());
        board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼  ♠    ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(1, mem.getHiddenBombs().size());
        AtomicInteger timer = mem.getHiddenBombs().get(board.get(3, 1));
        Assert.assertNotNull(timer);
        Assert.assertEquals(4, timer.get());
        board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼  ♠    ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(1, mem.getHiddenBombs().size());
        timer = mem.getHiddenBombs().get(board.get(3, 1));
        Assert.assertNotNull(timer);
        Assert.assertEquals(3, timer.get());
        board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼  ♠    ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(1, mem.getHiddenBombs().size());
        timer = mem.getHiddenBombs().get(board.get(3, 1));
        Assert.assertNotNull(timer);
        Assert.assertEquals(2, timer.get());
        board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼  ♠    ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(1, mem.getHiddenBombs().size());
        timer = mem.getHiddenBombs().get(board.get(3, 1));
        Assert.assertNotNull(timer);
        Assert.assertEquals(1, timer.get());
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼  ♠    ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(0, mem.getHiddenBombs().size());
    }

    @Test
    public void hiddenBombsChopperTest() {
        LongMemory mem = new LongMemory();
        Memory board = BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼   2&  ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼   3&  ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼   4&  ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼☺  5&  ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(0, mem.getHiddenBombs().size());
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼   &   ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼   &   ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼   &   ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼☺  &   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(4, mem.getHiddenBombs().size());
        Assert.assertTrue(mem.getHiddenBombs().entrySet().stream().anyMatch(e -> e.getValue().get() == 1));
        Assert.assertTrue(mem.getHiddenBombs().entrySet().stream().anyMatch(e -> e.getValue().get() == 2));
        Assert.assertTrue(mem.getHiddenBombs().entrySet().stream().anyMatch(e -> e.getValue().get() == 3));
        Assert.assertTrue(mem.getHiddenBombs().entrySet().stream().anyMatch(e -> e.getValue().get() == 5));
    }

    @Test
    public void myActivePerksTest() {
        LongMemory mem = new LongMemory();
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼i☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ♥   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(0, mem.getMe().getBoomImmuneTimer());
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼☺☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ♥   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(Player.PERK_TIME, mem.getMe().getBoomImmuneTimer());
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ♥   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(Player.PERK_TIME - 1, mem.getMe().getBoomImmuneTimer());
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼☺☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ♥   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(Player.PERK_TIME - 2, mem.getMe().getBoomImmuneTimer());
    }

    @Test
    public void bombRadiusIncreasePerkTest() {
        LongMemory mem = new LongMemory();
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼+☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ♥   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(0, mem.getMe().getBoomRadiusTimer());
        Assert.assertEquals(Player.DEFAULT_BOMB_RADIUS, mem.getMe().getBoomRadius());
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼☺☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ♥   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(Player.PERK_TIME, mem.getMe().getBoomRadiusTimer());
        Assert.assertEquals(Player.DEFAULT_BOMB_RADIUS + Player.BOMB_RADIUS_INCREASE, mem.getMe().getBoomRadius());
        BaseTest.board("☼☼☼☼☼☼☼☼☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼  ☺    ☼" //
                + "☼ ☼ ☼ ☼ ☼" //
                + "☼       ☼" //
                + "☼ ☼ ☼#☼#☼" //
                + "☼   ♥   ☼" //
                + "☼☼☼☼☼☼☼☼☼", mem);
        Assert.assertEquals(Player.PERK_TIME - 1, mem.getMe().getBoomRadiusTimer());
        Assert.assertEquals(Player.DEFAULT_BOMB_RADIUS + Player.BOMB_RADIUS_INCREASE, mem.getMe().getBoomRadius());
    }
}
